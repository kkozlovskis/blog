@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @foreach($article as $user_article)
                <form action="/edit_article/{{ $user_article->id }}" method="post">
                    <h3>Edit title:</h3>
                    <input type="text" class="form-control" name="title" id="title" value="{{$user_article->title}}">
                    <h3>Edit article:</h3>
                    <textarea class="form-control" rows="3"  name="article" id="article" placeholder="Article">{{$user_article->article}}</textarea><br>
                    <button type="submit" class="btn btn-primary">Save article</button>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form><hr>
            @endforeach
        </div>
    </div>
</div>
@endsection

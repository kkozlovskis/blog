@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
                <form action="/add_article" method="post">
                    <h3>Add title:</h3>
                    <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                    <h3>Add article:</h3>
                    <textarea class="form-control" rows="3"  name="article" id="article" placeholder="Article"></textarea><br>
                    <button type="submit" class="btn btn-primary">Add article</button>
                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                </form><hr>
            @foreach($articles as $article)
                <div class="panel panel-default">
                        <div class="panel-heading">{{ $article->title }}</div>
                        <div class="panel-body">{{ $article->article }}</div>
                </div>
                <p>Author:{{ $article->user->name }} | {{ $article->created_at }}</p>
                @if (Auth::user()->id == $article->author_id)
                <a href="/delete_article/{{ $article->id }}" type="button" class="btn btn-danger">Delete article</a>
                <a href="/get_edit_article/{{ $article->id }}" type="button" class="btn btn-info">Edit article</a>
                @endif
                <hr>
            @endforeach
        </div>
    </div>
</div>
@endsection

<?php

namespace App\Http\Controllers;
use App\Articles;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

class ArticleController extends Controller
{
    public function add_article(Request $request) {
        
        $title = $request['title'];
        $article = $request['article'];
        
        $articles = new Articles();
        $articles->title = $title;
        $articles->article = $article;
        $articles->author_id = Auth::user()->id;
        
        $articles->save();
        return redirect()->back();
    }
    
    public function delete_article($article_id){
        $article = Articles::where('id', $article_id)->first();
//        Checks if user is post author
        if (Auth::user()->id != $article->author_id) {
            return redirect()->back();
        }
        $article->delete();
        return redirect()->back();
    }
//    gets article for the edit view
    public function get_edit_article($article_id){
        $article = Articles::where('id', $article_id)->get();
        return view('edit_article', ['article' => $article]);
    }
    
    public function edit_article(Request $request,$article_id){
        $article = $request['article'];

        $articles = Articles::find($article_id);
        $articles->article = $article;

        $articles->update();
        return redirect()->route('home');
    }
}

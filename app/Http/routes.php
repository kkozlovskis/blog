<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/add_article', 'ArticleController@add_article');

Route::get('/delete_article/{article_id}', 'ArticleController@delete_article');

Route::get('/get_edit_article/{article_id}', 'ArticleController@get_edit_article');

Route::post('/edit_article/{article_id}', 'ArticleController@edit_article');